package com.zaqqasatrio.appnotesroutes

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity(), View.OnClickListener {

    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        btnGps.setOnClickListener(this)
        btnMaps.setOnClickListener(this)

        if (FirebaseAuth.getInstance().uid.isNullOrEmpty()) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK
            )
            this.startActivity(intent)
        }

        btnLogoff.setOnClickListener {
            fbAuth.signOut()
            val intent = Intent(this, DashboardActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.startActivity(intent)
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnGps -> {
                var intent = Intent(this, GpsActivity::class.java)
                startActivity(intent)
            }
            R.id.btnMaps -> {
                var intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
            }
        }
    }
}